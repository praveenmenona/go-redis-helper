package main

import (
	"go-redis-helper/internal/server"

	"go.ftdr.com/go-utils/common/errors"
	"go.ftdr.com/go-utils/common/logging"
)

func main() {
	err := server.RunServer()
	if err != nil {
		logging.Log.WithField("Error", err).Fatal(errors.ServerQuitError)
	}
}
