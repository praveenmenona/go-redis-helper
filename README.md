# Template Microservice
The template microservice is a base project that has several handy features and examples for writing 
microservices. It comes complete with configurations, logging, protobuf request handling, and routing.

## Configuration
### Rename
The microservice will need to be renamed, and that includes creating a repo, renaming directories and references,
as well as routes. Run `./clone.sh` to rename the project to the target project name.

### Tokens 
You'll need to set up your gitlab token in env vars. First, verify that you have access to the necessary private dependencies/repositories, then create a gitlab token with the permission `read_repository`. You can then save this token to your system's environment, as detailed below. If you will be using Azure devops, you'll need `read_registry` too.

These are the env vars you need:
```
GITLAB_TOKEN=<token>

```
If you do not have an env var named exactly as it appears above, it will not be successfully passed through into the microservice, and you will not be able to run it. This can be accomplished with `export GITLAB_TOKEN=<token>` and can be verified with `echo $GITLAB_TOKEN`.

### Config Values
The preferred method of config is with environment variables. The template does, however, support the legacy config files that use `MY_POD_NAMESPACE` and `ENV_FILE`. 

#### Supported Config Options

| Name                               | Description                                    |
|------------------------------------|------------------------------------------------|
|`$PORT`               | Listener port for the service                  |
|`$LOG_FILE_NAME`      | Name of logging file (Default server.log)      |
|`$LOG_LEVEL`          | Log Level (Default debug)                      |
|`$LOG_CONSOLE_ENABLED`| Write to stdout (Default true)                 |

### Go Mods
This template uses [Go Mods](https://github.com/golang/go/wiki/Modules) to manage dependencies. All 
external dependencies are in the `go.mod` file. There is no need to run the command `go get`, just import any 
modules you would like to use into the project, and the package will need to be added to the `go.mod`
file in the root directory. In the `require` block, we specify the package and the version. 

There are several ways to get Go Mods running in your local environment. 
- Unix Command
    ``` 
    $ export GO111MODULE=on
    $ go mod download
    ```
- Set in GoLand
    1. Go to System Preferences for GoLand
    2. Select Go
    3. Select Go Modules
    4. Check "Enable Go Modules"
    
### Environment
The environment is initialized in the `internal/config` package. In order for this to work, 
the environment file variable file needs to be set as `ENV_FILE=path/to/your/config.env`.
This can be done in two ways:
- Unix Command
    ```
    $ export ENV_FILE=path/to/your/config.env
    ```
- Set in GoLand Configs
    1. Click on your the GO BUILD MAIN.GO dropdown in upper right corner (left of play button)
    2. Select Edit Configurations
    3. Add `ENV_FILE=path/to/your/config.env` to your environment

### Logging
The logging implements the [Logrus](https://github.com/sirupsen/logrus) package.
We use a custom formatter in `formatter.go`. In addition to level and time, the customer formatter
 also provides the file, method name, and line number of the caller. Here is some sample log output:

`[INFO]   2019-07-30 11:33:09 [pkg/logging/loggingConfig.go] [logging.(*LoggingConfig).Initialize: 31] - Logging initialized`

The formatter pads the level of logging to a uniform constant. This constant can be found as a `const` 
that is unexported from the `logging` package, but can be found in `formatter.go`. 

To use the logger, the `logging` package must be used. To add to the logs, use `logging.Log`
followed by the level to log at. Here is an example:
```
// Log a warning
logging.Log.Warn("This is a warning")

// Log an error example, err is of type error here
logging.Log.Errorf("This is an error: %v", err)
```

In addition to logging messages, Logrus has the ability to log data with key-value pairs.
Keys needs to be strings and the value is accepted as `interface{}`
Here is an example:
```
// Log a single data field
logging.Log.withField("Key", "value").Info("The log message")


// Log an error
logging.Log.withField("Error", err).Error("Some error context")

// Log multiple data fields
dataFields = map[string]interface{}{
    "key1": "value1",
    "key2": 2,
}
logging.Log.WithFields(dataFields).Debug("The log message")

// Additionally you can create default fields
log := logging.Log..WithField("Topic", config.TopicName)
...
log.WithField("Error", err).Error("a error occured")

```
See https://github.com/sirupsen/logrus for more options

### Testing
In Go, tests belong in the same directory as file they are testing, and append the filename with `_test.go`.
For example, to write tests for a file `pkg/example/example.go`, the test file would be `pkg/example/example_test.go`

In order to test all test files in a certain directory manually, execute the following
command from the desired directory to be tested:
```
$ go test ./...
```

Alternatively, there are targets in the `Makefile` for testing. Here is the full list of testing targets:
* `make test`
* `make test-bench`
* `make test-coverage`
* `make test-coverage-tools`
* `make test-default`
* `make test-race`
* `make test-short`
* `make test-verbose`

Alternatively, one can use GoLand to run the tests. This is very useful for setting breakpoints
and debugging certain functions and finding the potential reasons for test failures.

## Endpoints
### Server
 If there are any web server configurations needed for the project, they should be added to the `WebServerConfig` struct located in 
`pkg/internal/config/config.go`. This should include any configurations necessary for a WebServer, such as the configs for
a database. The `Server` struct in `pkg/server/server.go` should contain the types that need to be initialized by the
`WebServer`, so the actual database connections, the router, etc, can be added there and used as needed.


### Routing
The router we are using is the [Gorilla Mux](https://github.com/gorilla/mux) router to serve the http routes.
The router is started in `internal/server/server.go` and the routes are defined in the `internal/server/router.go`.
There are a couple steps in order to serve a new route.
1. Generate request and response marshaling and unmarshaling code.
    1. Edit the `service.proto`. The package can be named whatever, in this case its just `pb`.
    2. Define any requests or responses. Example:
        ```
        message GreetingRequest{
            string greeting = 1;
            string guest = 2;
        }
        ```
    3. Once done with the proto file, generate the request and response code with the following command:
        ```
        $ make generate-grpc-service
        ```
2. Add routes to `internal/server/router.go` (in this example `r` is of type `mux.Router`)
    1. Define a handler for the new route
        1. The handler takes two parameters: a `writer http.ResponseWriter` and an `request *http.Request`, and has no returns.
        2. Set the `Content-Type` of the response header to `x-protobuf`
        3. If the request has a body, unmarshal the body using the generated code.
            ```
            body := ioutil.ReadAll(request.Body)
            greetingRequest := proto.Unmarshal(&pb.GreetingRequest{}, body)
            ```
        4. Do any processing with the request.
        5. Marshall the response.
            ```
            helloResponse := &pb.HelloResponse{
                Greeting: "Hello to whomever!"
            }
            marshalledResponse := proto.Marshal(helloResponse)
            ```
        5. The handlers do not return anything because responses are written to the response writer instead of a return.
            ```
            writer.Write(marshalledResponse)
            ```
    2. Register a route for the endpoint to the handler that will be used
        ```
        r.HandleFunc("your/route", handlerFunctionName).Methods("PUT", "POST")
        ```
        1. The `Methods` function allows for any http verbs used for that particular route.
        2. Route params can be achieved by adding curly braces to the variable in the route in `router.go`, and accessed
        via `mux.Vars` in the handler function.
            1. In your route: `"hello/{guestName}"`
            2. In your handler: `mux.Vars(r)["guestName"]`
        3. The route needs to be named. The name of the route and the authorization scope for the particular route need to be added 
        to the `map[string]string` passed into the `AuthMiddleware` middleware handler. 
        
## Pipeline
### Docker

The microservice is hosted in a [Docker](https://docs.docker.com) container. In order to run the microservice locally, Docker must first be
installed and set up.

**Please note that the dockerfile for this template uses a base image that is not currently public. In order to run this template using docker, you will first need to log-in to docker by using `docker login registry.gitlab.com` from the command line, then entering your gitlab credentials.**

 There is a target in the `Makefile` for building the image: `make build`. This will create the 
image for the Docker container. Alternatively, if you wish to build and run the container, use `make run`. This will 
rebuild the image and run the Docker container. This command uses port mapping, so it exposes `:8080`. With this mapping 
restful calls to the microservice endpoints on `localhost:8080` will be served by the container. Alternatively, one can 
have more control over the deploy process as well. Here is how to manually to expose a different port:
```
$ make build
$ docker image ls
> REPOSITORY                TAG                                IMAGE ID            CREATED             SIZE
  microservice-template     0.0.1                              6288ebeabc99        5 minutes ago       857MB
$ docker run -p <host port>:<container port> microservice-template:0.0.1
```
The Docker uses a [multi stage](https://docs.docker.com/develop/develop-images/multistage-build/) build to build the image. 
The build happens in 2 stages:
    1. Build the application.
    2. Copy the application binary and run the binary.

The naming of the files follows the convention of `<applicationname>-<hostlocation>.toml`. The host locations are `beta`, `dev`, 
`prod`, `stage`, `staging`, and `test`.

### CI/CD 
`.gitlab-ci.yml` is defined in the root directory of this template. It contains a reference to a #devops managed pipeline that will work for basic microservices based off this template. 
This is what it contains:
```
include:
  - remote: "https://gitlab.com/api/v4/projects/14359805/repository/files/.go-microservices.gitlab-ci.yml/raw?ref=master&private_token=EcCWHhYVzbSzcmGxpDyC&.yml"
``` 

When you commit it will run `lint`, `fmt` and `test` before rolling out to the dev environment. We are in the process of defining an `integration` step in the pipeline also. This step will be for developers to define integration tests that will be run after deployment to dev. Stay tuned.

## Profiling
Go has a built in profiling tool called `pprof`. There is an endpoint in the template that will turn on the profiler and you can then use `go tools pprof` to monitor the usage. For example to turn on the profiler, and create a visual tree of the memory usage in a webpage start the microservice locally and run:
```
curl -iX PUT https://localhost:8080/debug/pprof/enable -k -H "Authorization: Bearer $HELLO_TOKEN"
go tool pprof https+insecure://localhost:8080/debug/pprof/heap
```
The pprof CLI will start and you can type `web` to start a browser window to view the usage.
```
go tool pprof https+insecure://localhost:8080/debug/pprof/heap
Fetching profile over HTTP from https+insecure://localhost:8080/debug/pprof/heap
Saved profile in /Users/ggrigsby/pprof/pprof.application.alloc_objects.alloc_space.inuse_objects.inuse_space.007.pb.gz
File: application
Type: inuse_space
Time: Nov 14, 2019 at 5:21pm (MST)
Entering interactive mode (type "help" for commands, "o" for options)
(pprof) web
```

### Turning profiling on and off
Please note that profiling consumes resources. As such we recommend keeping it off until it is needed. Then we recommend turning it off afterwards.
On
```
PUT /debug/pprof/enable
no body
response 400 or 204
```

Off
```
PUT /debug/pprof/enable
no body
response 400 or 204
```

### Deployment Config
The deployment yaml config files follow the convention set by DevOps for proper linking of environment variables and other configuration settings.
Please see [here](https://ftdr.quip.com/4C8pAlrcpSZH/DEVOPS-Service-Config-Reference) for more details. 
