#!/bin/bash
set -euo pipefail
set -x
echo 
echo 
echo "Hello, "$USER".  This script will copy this template into a new directory and rename your imports." 
read -p "Enter the name you want to call this service: " NEW_PROJECT_NAME 
echo
echo
echo
PRIOR_MICROSERVICE_NAME=${PWD##*/}
echo "Renaming $PRIOR_MICROSERVICE_NAME to $NEW_PROJECT_NAME"
cd ..
echo "Cloning Template"
cp -r "$PRIOR_MICROSERVICE_NAME/" $NEW_PROJECT_NAME
ls -a
cd $NEW_PROJECT_NAME
echo "Updating Imports"
UP_PRIOR=`printf '%s\n' "$PRIOR_MICROSERVICE_NAME" | awk '{ print toupper($0) }'`
UP_NAME=`printf '%s\n' "$NEW_PROJECT_NAME" | awk '{ print toupper($0) }'`
#check for mac
if [[ "$OSTYPE" == "darwin"* ]]; then
    export LC_ALL=C
    find . -type f -not -path './third_party/*' -exec sed -i '' -e "s/${PRIOR_MICROSERVICE_NAME}/${NEW_PROJECT_NAME}/g" {} \;
    find . -type f -not -path './third_party/*' -exec sed -i '' -e "s/${UP_PRIOR}/${UP_NAME}/g" {} \;
    unset $LC_ALL
else #windows WSL
    find . -type f -not -path './third_party/*' -exec sed -i "s/${PRIOR_MICROSERVICE_NAME}/${NEW_PROJECT_NAME}/g" {} \;
    find . -type f -not -path './third_party/*' -exec sed -i "s/${UP_PRIOR}/${UP_NAME}/g" {} \;
fi
echo "Reconfiguring Git Links"
rm -Rf .git 2>/dev/null
git init
git add .
git commit -am "Initial Commit"
echo "Done!"
