FROM registry.gitlab.com/ftdr/software/go-protoc:alpine AS builder
COPY . /app

ENV GOARCH=amd64
ENV GOOS=linux
ENV CGO_ENABLED=0
ENV GOPRIVATE=go.ftdr.com*,golang.frontdoorhome.com*

ARG GITLAB_ID
ARG GITLAB_TOKEN
RUN git config --global url."https://${GITLAB_TOKEN}:${GITLAB_TOKEN}@gitlab.com/ftdr".insteadOf "https://gitlab.com/ftdr"

WORKDIR /app

RUN go mod download
RUN make build

FROM alpine

COPY --from=builder /app/bin/application /bin/application

EXPOSE 50051
ENTRYPOINT ["/bin/application"]
