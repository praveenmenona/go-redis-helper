module go-redis-helper

go 1.13

replace golang.org/x/text => golang.org/x/text v0.3.3 // for dependency scanner

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/garyburd/redigo v1.6.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gomodule/redigo v1.8.3
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/pkg/errors v0.8.1
	github.com/pkg/profile v1.3.0
	github.com/rafaeljusto/redigomock v2.4.0+incompatible
	github.com/stretchr/testify v1.6.1
	go.ftdr.com/go-utils/common v0.1.134
	golang.frontdoorhome.com/software/protos v1.0.438
)
