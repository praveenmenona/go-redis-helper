package config

import (
	"fmt"
	"go.ftdr.com/go-utils/common/logging"
	"os"
	"time"

	"github.com/kelseyhightower/envconfig"

	"github.com/joho/godotenv"
)

const (
	envPrefix = "GO-REDIS-HELPER"
)

//WebServerConfig ...
type WebServerConfig struct {
	Port       string `required:"true" split_words:"true" default:"50051"`
	EnableAuth bool
}

// RedisConfig hold the redis connection configs
type RedisConfig struct {
	Network                          string        `envconfig:"REDIS_HOST" required:"true" default:"127.0.0.1"`
	Port                             string        `envconfig:"REDIS_PORT" required:"true" default:"6379"`
	MaxActive                        int           `split_words:"true" default:"5000"`
	ConnectTimeout                   time.Duration `split_words:"true" default:"50s"`
	RequestTimeout                   time.Duration `split_words:"true" default:"1s"`
	ConnectionRetryLimit             int           `split_words:"true" default:"5"`
	ConnectionRetrySleep             time.Duration `split_words:"true" default:"1m"`
	ConnectionIntermittentRetrySleep time.Duration `split_words:"true" default:"5m"`
	Enabled                          bool          `default:"true"`
}

type ServiceConfig struct {
	Redis                        *RedisConfig  `required:"true"`
	OnHandAvailabilityExpiration time.Duration `split_words:"true" default:"30m"`
}

type AppConfig struct {
	WebServer     *WebServerConfig
	ServiceConfig *ServiceConfig
	Logging       *logging.Config
}

//FromEnv ...
func FromEnv() (cfg *AppConfig, err error) {
	fromFileToEnv()

	cfg = &AppConfig{}

	err = envconfig.Process("", cfg)

	if err != nil {
		return nil, err
	}

	return cfg, nil
}

func fromFileToEnv() {
	cfgFilename := os.Getenv("ENV_FILE")
	if cfgFilename != "" {
		err := godotenv.Load(cfgFilename)
		if err != nil {
			fmt.Println("ENV_FILE not found. Trying MY_POD_NAMESPACE")
		}
		return
	}

	loc := os.Getenv("MY_POD_NAMESPACE")

	cfgFilename = fmt.Sprintf("../../etc/config/config.%s.env", loc)

	err := godotenv.Load(cfgFilename)
	if err != nil {
		fmt.Println("No config files found to load to env. Defaulting to environment.")
	}

}
