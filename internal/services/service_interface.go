package services

import (
	"context"
	"go-redis-helper/internal/model"
)

// PaymentService ...
type PaymentCacheService interface {
	CachePayment(ctx context.Context, paymentRequest *model.PaymentRequest) string

	GetPaymentFromRedis(ctx context.Context, paymentId string) (paymentResponse *model.PaymentResponse)

	Initialize()
}
