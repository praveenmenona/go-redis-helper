package services

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"go-redis-helper/internal/config"
	"go-redis-helper/internal/model"
	"go-redis-helper/internal/services/cache"
	"go.ftdr.com/go-utils/common/logging"
)

type PaymentService struct {
	Cache         cache.RedisCache
	serviceConfig *config.ServiceConfig
}

// NewPaymentService creates a new PaymentServicer
func NewPaymentService(serviceConfig *config.ServiceConfig, redis cache.RedisCache) PaymentCacheService {
	return &PaymentService{
		Cache:         redis,
		serviceConfig: serviceConfig,
	}
}

// Initialize fully initialize the service
func (s *PaymentService) Initialize() {
	s.Cache = cache.NewRedisCache(s.serviceConfig.Redis)
}

func (s *PaymentService) createPaymentMethodID() string {
	paymentId := uuid.New().String()
	return paymentId
}

func (s *PaymentService) CachePayment(ctx context.Context, paymentRequest *model.PaymentRequest) string {
	log := logging.GetTracedLogEntry(ctx)
	var id string
	if s.Cache.IsEnabled() && paymentRequest.ToByteArray(ctx) != nil {
		timeout := int(s.serviceConfig.OnHandAvailabilityExpiration.Seconds())
		id = s.createPaymentMethodID()
		if err := s.Cache.SetEx(ctx, id, paymentRequest.ToByteArray(ctx), timeout); err != nil {
			log.WithError(err).WithField("Key", id).Info("unable to cache payment request")
		}
	}
	return id
}

func (s *PaymentService) GetPaymentFromRedis(ctx context.Context, paymentId string) (paymentResponse *model.PaymentResponse) {
	log := logging.GetTracedLogEntry(ctx)
	if !s.Cache.IsEnabled() {
	}

	cachedPayment := &model.PaymentResponse{}

	data, err := s.Cache.Get(ctx, paymentId)
	if err != nil {
		log.WithError(err).Warn("error when retrieving cached payment from Redis cache")
	} else if data != nil {
		err = json.NewDecoder(bytes.NewReader(data)).Decode(cachedPayment)
		if err != nil {
			log.WithField("Error", err).Error(err.Error())
			return
		}
	}
	fmt.Println("cached Payment: ", cachedPayment)
	return cachedPayment
}
