package cache

import "context"

// RedisCache is the interface for accessing Redis
type RedisCache interface {
	IsEnabled() bool

	Ping() error

	Get(ctx context.Context, key string) ([]byte, error)

	SetEx(ctx context.Context, key string, value []byte, timeout int) error

	Exists(ctx context.Context, key string) (bool, error)

	Delete(ctx context.Context, key string) error

	GetKeys(ctx context.Context, pattern string) ([]string, error)
}
