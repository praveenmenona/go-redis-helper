package cache

import (
	"context"
	"fmt"
	"go-redis-helper/internal/config"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
	"go.ftdr.com/go-utils/common/logging"

	redisErrors "go-redis-helper/pkg/errors"
)

// Redis is the cache type, utilizes pooling for more connections
type Redis struct {
	pool              *redis.Pool
	config            *config.RedisConfig
	enabled           bool
	connectionRetries int
	err               error
}

// NewRedisCache ...
func NewRedisCache(config *config.RedisConfig) (redisCache *Redis) {
	logging.Log.Info("attempting to connect to Redis cache")
	redisCache = InitializeRedisCache(config)
	if redisCache.IsEnabled() {
		err := redisCache.Ping()
		if err != nil {
			logging.Log.Info("could not connect to Redis cache")
		} else {
			logging.Log.Info("successfully connected to Redis cache")
		}
	} else {
		logging.Log.Info("Redis is disabled from configs, no retry occurrences")
	}

	return redisCache
}

// InitializeRedisCache returns a Redis cache
func InitializeRedisCache(config *config.RedisConfig) *Redis {
	r := &Redis{
		config: config,
	}

	r.enabled = r.config.Enabled
	r.pool = &redis.Pool{
		Dial: func() (redis.Conn, error) {
			connectTimeout := redis.DialConnectTimeout(config.ConnectTimeout)
			readTimeout := redis.DialReadTimeout(config.RequestTimeout)
			writeTimeout := redis.DialWriteTimeout(config.RequestTimeout)

			address := fmt.Sprintf("%s:%s", config.Network, config.Port)

			connection, err := redis.Dial("tcp", address, connectTimeout, readTimeout, writeTimeout)
			if err != nil {
				logging.Log.WithError(err).Warn("failed to dial Redis")
				return nil, err
			}
			return connection, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			if err != nil {
				logging.Log.WithError(err).Info("failed TestOnBorrow for Redis")
			}
			return err
		},
		MaxActive: config.MaxActive,
		Wait:      false,
	}

	return r
}

// IsEnabled will check if Redis is enabled - the initial value is set from configs
func (r *Redis) IsEnabled() bool {
	return r.enabled
}

// Ping will ping the database to check connection health if it is enabled
func (r *Redis) Ping() error {
	if !r.IsEnabled() {
		return nil
	}

	pingErr := r.ping()
	if pingErr != nil {
		go r.intermittentRetry()
		return errors.Wrap(pingErr, redisErrors.CachePingError.Error())
	}

	return nil
}

func (r *Redis) ping() error {
	conn := r.pool.Get()
	defer closeConnection(conn)

	_, r.err = redis.String(conn.Do("PING"))
	if r.err != nil {
		logging.Log.WithError(r.err).Warn("failed to ping Redis")
		return r.err
	}

	return nil
}

// Get will search for a key and return the data found
// If no data is found, will return nil
func (r *Redis) Get(ctx context.Context, key string) ([]byte, error) {
	log := logging.GetTracedLogEntry(ctx)

	conn := r.pool.Get()
	defer closeConnection(conn)

	start := time.Now()

	var data []byte
	data, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		if err == redis.ErrNil {
			log.WithField("Key", key).WithField("Duration", time.Since(start)).Debug("could not find key")
			return nil, nil
		}
		log.WithError(err).WithField("Key", key).Warn("failed to get key")
		r.err = err
		go r.intermittentRetry()
		return data, errors.Wrap(r.err, redisErrors.CacheGetError.Error())
	}

	log.WithField("Key", key).WithField("Duration", time.Since(start)).Debug("successfully got key")
	return data, nil
}

// SetEx will take a key, value, and timeout and set them with a ttl
func (r *Redis) SetEx(ctx context.Context, key string, value []byte, timeout int) error {
	log := logging.GetTracedLogEntry(ctx)

	conn := r.pool.Get()
	defer closeConnection(conn)

	log.WithField("Key", key).WithField("Timeout", timeout).Debug("setting key")
	_, r.err = conn.Do("SETEX", key, timeout, value)
	if r.err != nil {
		log.WithError(r.err).WithField("Key", key).Warn("failed to set key with ttl")
		go r.intermittentRetry()
		return errors.Wrap(r.err, redisErrors.CacheSetExError.Error())
	}
	return nil
}

// Exists will check for a key in the cache, and return true if found
func (r *Redis) Exists(ctx context.Context, key string) (bool, error) {
	log := logging.GetTracedLogEntry(ctx)

	conn := r.pool.Get()
	defer closeConnection(conn)

	var success bool
	success, r.err = redis.Bool(conn.Do("EXISTS", key))
	if r.err != nil {
		log.WithError(r.err).WithField("Key", key).Warn("failed to check key existence")
		go r.intermittentRetry()
		return success, errors.Wrap(r.err, redisErrors.CacheExistenceError.Error())
	}
	return success, nil
}

// Delete will delete a key from the cache
func (r *Redis) Delete(ctx context.Context, key string) error {
	log := logging.GetTracedLogEntry(ctx)

	conn := r.pool.Get()
	defer closeConnection(conn)

	_, r.err = conn.Do("DEL", key)
	if r.err != nil {
		log.WithError(r.err).WithField("Key", key).Warn("failed to delete key")
		go r.intermittentRetry()
		return errors.Wrap(r.err, redisErrors.CacheDeleteError.Error())
	}
	return nil
}

// GetKeys will check for a pattern and retrieve keys matching the pattern
func (r *Redis) GetKeys(ctx context.Context, pattern string) ([]string, error) {
	log := logging.GetTracedLogEntry(ctx)

	conn := r.pool.Get()
	defer closeConnection(conn)

	iter := 0
	var keys []string
	for {
		var arr []interface{}
		arr, r.err = redis.Values(conn.Do("SCAN", iter, "MATCH", pattern))
		if r.err != nil {
			log.WithError(r.err).WithField("Pattern", pattern).Warn("failed to get keys for pattern")
			go r.intermittentRetry()
			return keys, errors.Wrap(r.err, redisErrors.CacheExistenceError.Error())
		}

		iter, _ = redis.Int(arr[0], nil)
		k, _ := redis.Strings(arr[1], nil)
		keys = append(keys, k...)

		if iter == 0 {
			break
		}
	}

	return keys, nil
}

func closeConnection(conn redis.Conn) {
	err := conn.Close()
	if err != nil {
		logging.Log.WithError(err).Warn("failed to close connection")
	}
}

// intermittentRetry will try to reconnect to Redis. If Redis comes back up, it will re-enable Redis to be used.
// If it cannot connect after the limit is reached, it will sleep for ConnectionIntermittentRetrySleep and try again later
func (r *Redis) intermittentRetry() {
	logging.Log.Info("disabling Redis; retrying connection")
	r.enabled = false
	for !r.enabled {
		if successfulConnection := r.retryConnection(); !successfulConnection {
			logging.Log.Infof("reconnection failed after %v attempts; waiting %v to retry connection", r.config.ConnectionRetryLimit, r.config.ConnectionIntermittentRetrySleep)
			r.connectionRetries = 0
			time.Sleep(r.config.ConnectionIntermittentRetrySleep)
		} else {
			logging.Log.Info("Redis reconnect retry succeeded - enabling Redis")
			r.enabled = true
		}
	}
}

// retryConnection pings Redis on a set timeout for the configured number of retry attempts. Every unsuccessful attempt
// sleeps for ConnectionRetrySleep before trying to ping again
func (r *Redis) retryConnection() (successfulConnection bool) {
	for r.connectionRetries < r.config.ConnectionRetryLimit {
		r.connectionRetries++
		logging.Log.WithField("RetryAttempt", r.connectionRetries).Info("retrying Redis connection")
		if r.err = r.ping(); r.err != nil {
			logging.Log.WithError(r.err).WithField("RetryAttempt", r.connectionRetries).Warnf("Redis connection retry failed; waiting %v", r.config.ConnectionRetrySleep)
			time.Sleep(r.config.ConnectionRetrySleep)
		} else {
			r.err = nil
			r.connectionRetries = 0
			return true
		}
	}

	logging.Log.Warn("Redis connection retry limit reached")
	return false
}
