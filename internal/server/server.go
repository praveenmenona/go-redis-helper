package server

import (
	"go-redis-helper/internal/config"
	"net/http"

	"go.ftdr.com/go-utils/common/logging"
)

//Server ...
type Server struct {
	Configuration *config.WebServerConfig
	Router        *Router
}

//NewServer ...
func NewServer(config *config.WebServerConfig) *Server {
	server := &Server{
		Configuration: config,
		Router:        NewRouter(),
	}

	return server
}

//RunServer ...
func RunServer() (err error) {
	cfg, err := config.FromEnv()
	if err != nil {
		return err
	}

	err = logging.Initialize(cfg.Logging)
	if err != nil {
		return err
	}

	logging.Log.Infof("Starting HTTPS server on port %s", cfg.WebServer.Port)

	//client := redis.NewClient(&redis.Options{
	//	Addr:     os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT"),
	//	Password: "",
	//	DB:       0,
	//})

	//result, err := client.Ping().Result()

	//fmt.Println("testing redis connection", result, err)

	server := NewServer(cfg.WebServer)
	server.Router.InitializeRoutes(cfg.ServiceConfig)
	server.Router.InitializeMiddleware(cfg.WebServer.EnableAuth)

	err = http.ListenAndServe(":"+cfg.WebServer.Port, *server.Router)
	if err != nil {
		return err
	}

	return nil
}
