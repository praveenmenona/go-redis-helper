package server

import (
	"go-redis-helper/internal/config"
	"go-redis-helper/pkg/authmiddleware"
	"net/http"
	"net/http/pprof"

	"github.com/gorilla/mux"

	//authMW "go.ftdr.com/go-utils/common/authMiddleware"

	"go.ftdr.com/go-utils/common/middleware"
)

const (
	protobufContentType = "application/x-protobuf"

	helloScope = "hello"
)

var (
	//Microservices deployed to Istio environments do not implement Auth Middleware.
	routeAuthScopes = map[string]string{
		"healthcheck":   helloScope,
		"hello":         helloScope,
		"helloTo":       helloScope,
		"postHello":     helloScope,
		"pprof_enable":  helloScope,
		"pprof_disable": helloScope,
	}

	routeAcceptedContents = map[string]string{
		"healthcheck":   protobufContentType,
		"hello":         protobufContentType,
		"helloTo":       protobufContentType,
		"postHello":     protobufContentType,
		"pprof":         protobufContentType,
		"pprofc":        protobufContentType,
		"pprofp":        protobufContentType,
		"pprofs":        protobufContentType,
		"pproft":        protobufContentType,
		"pprofm":        protobufContentType,
		"pprof_enable":  protobufContentType,
		"pprof_disable": protobufContentType,
	}
	authExemptEndpoints = map[string]struct{}{
		"healthcheck": struct{}{},
		"pprof":       struct{}{},
		"pprofc":      struct{}{},
		"pprofp":      struct{}{},
		"pprofs":      struct{}{},
		"pproft":      struct{}{},
		"pprofm":      struct{}{},
	}
)

//Router ...
type Router struct {
	*mux.Router
}

//NewRouter ...
func NewRouter() *Router {
	return &Router{mux.NewRouter()}
}

//InitializeRoutes ...
func (r *Router) InitializeRoutes(serviceConfig *config.ServiceConfig) {
	(*r).HandleFunc("/debug/pprof/enable", ProfileEnabler()).
		Methods(http.MethodPut).
		Name("pprof_enable")

	(*r).HandleFunc("/debug/pprof/disable", ProfileDisabler()).
		Methods(http.MethodPut).
		Name("pprof_disable")

	(*r).HandleFunc("/debug/pprof/", pprof.Index).Name("pprof")
	(*r).HandleFunc("/debug/pprof/cmdline", pprof.Cmdline).Name("pprofc")
	(*r).HandleFunc("/debug/pprof/profile", pprof.Profile).Name("pprofp")
	(*r).HandleFunc("/debug/pprof/symbol", pprof.Symbol).Name("pprofs")
	(*r).HandleFunc("/debug/pprof/trace", pprof.Trace).Name("pproft")
	(*r).HandleFunc("/debug/pprof/heap", pprof.Handler("heap").ServeHTTP).Name("pprofm")

	(*r).HandleFunc("/healthcheck", HealthCheckHandler).
		Methods(http.MethodGet).
		Name("healthcheck")

	(*r).HandleFunc("/token/{paymentToken}", GetPayment(serviceConfig)).
		Methods(http.MethodGet).
		Name("hello-to")

	(*r).HandleFunc("/payment", CreatePayment(serviceConfig)).
		Methods(http.MethodPost).
		Name("post-hello")
}

//InitializeMiddleware ...
func (r *Router) InitializeMiddleware(auth bool) {
	r.Use(middleware.LoggingMiddleware)

	// Microservices deployed to Istio environments do not implement Auth Middleware.
	if auth {
		amw := authmiddleware.AuthMiddleWare{
			routeAuthScopes,
			authExemptEndpoints,
		}
		amw.SetAuthScopes(routeAuthScopes)
		r.Use(amw.Middleware)
	}

	vmw := middleware.ValidatorMiddleWare{}
	vmw.SetAcceptedContent(routeAcceptedContents)
	(*r).Use(vmw.Middleware)

	(*r).Use(middleware.QueryDecoderMiddleware)

	(*r).Use(middleware.Recovery)
}
