package server

import (
	"encoding/json"
	"fmt"
	"go-redis-helper/internal/config"
	"go-redis-helper/internal/model"
	"go-redis-helper/internal/services"
	"go-redis-helper/internal/services/cache"
	"net/http"

	"go.ftdr.com/go-utils/common/errors"
	"go.ftdr.com/go-utils/common/handlers"
	"go.ftdr.com/go-utils/common/logging"

	"github.com/gorilla/mux"
	"github.com/pkg/profile"
	commonpb "golang.frontdoorhome.com/software/protos/go/common"
)

var paymentToken string

var profiler interface{ Stop() }

// ProfileDisabler ...
func ProfileDisabler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logging.GetTracedLogEntry(r.Context())
		if profiler == nil {
			http.Error(w, "Profiler is not running", http.StatusBadRequest)
			return
		}
		profiler.Stop()
		profiler = nil
		w.WriteHeader(http.StatusNoContent)
		log.Info("Profiler disabled")
	})

}

// ProfileEnabler ...
func ProfileEnabler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logging.GetTracedLogEntry(r.Context())
		if profiler == nil {
			profiler = profile.Start(profile.MemProfile)
			w.WriteHeader(http.StatusNoContent)
			log.Info("Profiler enabled")
			return
		}
		http.Error(w, "Profiler is already running", http.StatusBadRequest)
	})

}

//HealthCheckHandler ...
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	log := logging.GetTracedLogEntry(r.Context())
	handlers.SetResponseHeader(w, handlers.ContentTypeJSON)

	healthCheckResponse := commonpb.HealthCheckResponse{
		Health: &commonpb.HealthCheck{
			Status:    "pass",
			Version:   "1",
			ReleaseID: "0.0.1",
			Notes:     []string{""},
			Output:    "failure description/stack trace",
			Details:   nil,
		},
	}

	response, err := json.Marshal(&healthCheckResponse)
	if err != nil {
		log.WithField("Error", err).Error(errors.MarshalError)
		http.Error(w, errors.MarshalError.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(response)
	if err != nil {
		log.WithField("Error", err).Error(errors.EncoderError)
		http.Error(w, errors.EncoderError.Error(), http.StatusInternalServerError)
	}
}

//PaymentGetHandler ...
func GetPayment(serviceConfig *config.ServiceConfig) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log := logging.GetTracedLogEntry(r.Context())
		handlers.SetResponseHeader(w, handlers.ContentTypeJSON)

		redisCache := cache.NewRedisCache(serviceConfig.Redis)
		PaymentService := services.NewPaymentService(serviceConfig, redisCache)

		params := mux.Vars(r)
		var paymentResponse *model.PaymentResponse

		fmt.Println("token: ", params["paymentToken"])

		paymentResponse = PaymentService.GetPaymentFromRedis(r.Context(), params["paymentToken"])

		//Get payment from Redis
		//payment, err := client.Get(params["paymentToken"]).Result()
		//if err != nil {
		//	fmt.Println(err)
		//}

		fmt.Println("response data: ", paymentResponse.Token)

		response, err := json.Marshal(&paymentResponse)
		if err != nil {
			log.WithField("Error", err).Error(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		_, err = w.Write(response)
		if err != nil {
			log.WithField("Error", err).Error(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

//PaymentPostHandler ...
func CreatePayment(serviceConfig *config.ServiceConfig) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log := logging.GetTracedLogEntry(r.Context())
		handlers.SetResponseHeader(w, handlers.ContentTypeJSON)

		redisCache := cache.NewRedisCache(serviceConfig.Redis)
		PaymentService := services.NewPaymentService(serviceConfig, redisCache)

		var paymentRequest model.PaymentRequest

		err := json.NewDecoder(r.Body).Decode(&paymentRequest)

		paymentId := PaymentService.CachePayment(r.Context(), &paymentRequest)

		fmt.Println("paymentId returned after caching: ", paymentId)

		if err != nil {
			log.WithField("Error", err).Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		paymentResponse := model.PaymentResponse{
			PaymentMethodId: paymentId,
			Type:            paymentRequest.Type,
			Token:           paymentRequest.Token,
		}

		response, err := json.Marshal(&paymentResponse)
		if err != nil {
			log.WithField("Error", err).Error(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//insert into redis
		//err = client.Set(paymentId, response, 0).Err()
		//if err != nil {
		//	fmt.Println(err)
		//}

		_, err = w.Write(response)
		if err != nil {
			log.WithField("Error", err).Error(errors.EncoderError)
			http.Error(w, errors.EncoderError.Error(), http.StatusInternalServerError)
		}
	}
}
