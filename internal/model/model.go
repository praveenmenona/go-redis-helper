package model

import (
	"context"
	"encoding/json"
	"go.ftdr.com/go-utils/common/logging"
)

type PaymentRequest struct {
	Type  string `bson:"type,omitempty" json:"type,omitempty"`
	Token string `bson:"token,omitempty" json:"token,omitempty"`
}

type PaymentResponse struct {
	PaymentMethodId string `bson:"payment_method_id,omitempty" json:"payment_method_id,omitempty"`
	Type            string `bson:"type,omitempty" json:"type,omitempty"`
	Token           string `bson:"token,omitempty" json:"token,omitempty"`
}

// ToByteArray marshals Payment Request  to byte array
func (pa PaymentRequest) ToByteArray(ctx context.Context) []byte {
	log := logging.GetTracedLogEntry(ctx)
	data, err := json.Marshal(pa)
	if err != nil {
		log.Warn("failed to marshal Payment Request")
		return nil
	}
	log.WithField("Data", data).Trace("marshaled Payment Request to byte array")
	return data
}
