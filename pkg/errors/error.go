package errors

var (
	// CacheDeleteError ...
	CacheDeleteError = ServerError{
		message: "unable to delete key from cache",
		code:    "CACHE_DELETE_ERROR",
	}

	// CacheExistenceError ...
	CacheExistenceError = ServerError{
		message: "unable to check key existence in cache",
		code:    "CACHE_EXISTENCE_ERROR",
	}

	// CacheGetError ...
	CacheGetError = ServerError{
		message: "unable to get key from cache",
		code:    "CACHE_GET_ERROR",
	}

	// CachePingError ...
	CachePingError = ServerError{
		message: "failed to ping cache",
		code:    "CACHE_PING_ERROR",
	}

	// CacheRefreshError ...
	CacheRefreshError = ServerError{
		message: "failed to refresh cache",
		code:    "CACHE_REFRESH_ERROR",
	}

	// CacheSetExError ...
	CacheSetExError = ServerError{
		message: "unable to set key with ttl in cache",
		code:    "CACHE_SET_EX_ERROR",
	}

	// CacheUnavailableError ...
	CacheUnavailableError = ServerError{
		message: "cache is not enabled",
		code:    "CACHE_UNAVAILABLE_ERROR",
	}
)
