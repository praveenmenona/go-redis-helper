package errors

// ServerError ...
type ServerError struct {
	message string
	code    string
}

// Error returns the error as a string
func (err *ServerError) Error() string {
	return err.code + ": " + err.message
}
